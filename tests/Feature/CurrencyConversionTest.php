<?php

namespace Tests\Feature;

use Tests\TestCase;

class CurrencyConversionTest extends TestCase
{
    public function testCurrencyConversion()
    {
        $response = $this->get('/api/currency/convert?source=USD&target=JPY&amount=1525');

        $response->assertStatus(200);
        $response->assertJson([
            'msg' => 'success',
        ]);

        $response->assertJsonStructure([
            'amount'
        ]);
    }
}

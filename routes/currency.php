<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CurrencyConversionController;

Route::get('/currency/convert', [CurrencyConversionController::class, 'convert']);
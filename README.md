# 匯率轉換 API

這是一個使用 Laravel 框架建立的匯率轉換 API。

## 安裝與設定

1. **Clone project**:

> git clone [專案 Git 連結]
> cd currency-conversion

2. **安裝依賴**:

> composer install

3. **設定環境變數**:

> 將 `.env.example` 檔案複製為 `.env`，並根據您的環境設定資料庫等設定。
> 
> cp .env.example .env

4. **產生密鑰**:

> php artisan key:generate

5. **啟動服務**:

> php artisan serve

## API 使用說明

##### GET */api/currency/convert*

**Request**
`source` (str): 必填，原始幣別。
`target` (str): 必填，目標幣別。
`amount` (int): 必填，轉換前金額。

**Response**
`msg` (str): success: 成功, failed: 請求參數錯誤/缺失。
`amount` (str): 轉換後金額，只在成功時返回。

## 執行測試

> 執行以下命令以進行單元測試：
> 
> php artisan test
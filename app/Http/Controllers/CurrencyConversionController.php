<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CurrencyConversionController extends Controller
{
    public function convert(Request $request)
    {
        // 取得請求資料
        $source = $request->input('source');
        $target = $request->input('target');
        $amount = $request->input('amount');

        // 匯率資料
        $rates = [
            'TWD' => ['TWD' => 1, 'JPY' => 3.669, 'USD' => 0.03281],
            'JPY' => ['TWD' => 0.26956, 'JPY' => 1, 'USD' => 0.00885],
            'USD' => ['TWD' => 30.444, 'JPY' => 111.801, 'USD' => 1]
        ];

        // 檢查是否為有效幣別
        if (!isset($rates[$source]) || !isset($rates[$target])) {
            return response()->json([
                'msg' => 'failed'
            ]);
        }

        // 檢查是否為有效數字
        if (!is_numeric($amount)) {
            return response()->json([
                'msg' => 'failed'
            ]);
        }
    
        // 貨幣轉換計算 (四捨五入小數後兩位)
        $convertedAmount = round($amount * $rates[$source][$target], 2);
        $formattedAmount = number_format($convertedAmount, 2);
    
        return response()->json([
            'msg' => 'success',
            'amount' => '$' . $formattedAmount
        ]);
    }
}
